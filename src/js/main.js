const burgerButton = {
    el: document.querySelector('.head__menu-btn'),
    _state: false,
    set state(x) {
      if (x === 'true' || x === true) {
        setTimeout(() => {
          this.el.dataset.active = true;
          this._state = true;
        }, 1);
      } else {
        this.el.dataset.active = false;
        this._state = false;
      }
    },
    get state() {
      return this._state
    }
  }
  
  burgerButton.el.addEventListener('click', (e) => {
    console.log(e)
    if (e.currentTarget.dataset.active === 'true') {
      burgerButton.state = false;
    } else {
      burgerButton.state = true;
    }
  });
  
  DOMTokenList.prototype.multiContains = function (arr) {
    let result = false;
    arr.forEach(e => {
      if (this.contains(e)) result = true
    });
    return result;
  }
  
  document.body.addEventListener('click', e => {
    if (!e.target.classList.multiContains(['head__menu', 'head__link']) && burgerButton.state === true) burgerButton.state = false;
  });
  
  window.addEventListener('resize', e => {
    if (window.innerWidth > 480) burgerButton.state = false;
  })